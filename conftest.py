import pytest
import requests
from search.models import Search
from rest_framework.test import APIClient


@pytest.fixture
def post_search():
    def response():
        response = requests.post("http://0.0.0.0:9000/api/search")
        return response

    return response

@pytest.fixture
def get_results():
    def response(search_id, currency):
        response = requests.get(f"http://0.0.0.0:9000/api/results/{search_id}/{currency}")
        return response

    return response