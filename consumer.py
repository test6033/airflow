import pika, json, os, django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "airflow.settings")
django.setup()

from search.models import Search

params = pika.URLParameters(
    "amqps://sdtevmrw:CER0nluE-IdH8vR_O0StXNjkOamBVONI@chimpanzee.rmq.cloudamqp.com/sdtevmrw"
)
connection = pika.BlockingConnection(params)
channel = connection.channel()
channel.queue_declare(queue="airflow")

def callback(ch, method, properties, body):
    try:
        data = json.loads(body)
        search = Search.objects.get(search_id=str(data["search_id"]))
        search.items = search.items + data["data"] if search.items else data["data"]
        search.status = data["status"]
        search.save()
    except Exception:
        search = None

channel.basic_consume(queue="airflow", on_message_callback=callback, auto_ack=True)
channel.start_consuming()
channel.close()
