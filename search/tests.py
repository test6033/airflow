import json
import pytest
from time import sleep
from rest_framework.test import APIClient
from conftest import post_search, get_results
from search.models import Search


class TestSearchEndpoint:

    endpoint = "/api/search"
    client = APIClient()
    
    def test_search_returns_search_id(self):
        response = self.client.post(
            "/api/search",
            format="json",
        )
        assert 'search_id' in response.data.keys()
    
    def test_created_search_status_is_pending(self):
        response = self.client.post(
            "/api/search",
            format="json",
        )
        assert response.data['status'] == "Pending"
          
    def test_get_data_by_incorrect_search_id(self, get_results):
        get_response = get_results("incorrect_search_id", "EUR")
        assert get_response.status_code == 404
    
    def test_get_data_by_incorrect_currency(self, get_results):
        get_response = get_results("incorrect_search_id", "incorrect_currency")
        assert get_response.status_code == 404
        
    def test_search_result_pending_after_thirty_seconds(self, post_search, get_results):
        response = post_search()
        response = json.loads(response.text)
        
        # request is pending result from provider-a 
        sleep(31)
        get_response = get_results(response['search_id'], "EUR")
        get_response = json.loads(get_response.text)
        assert get_response["status"] == "Pending"
        
        # request provider-b is completed result from provider-b 
        sleep(31)
        get_response = get_results(response['search_id'], "EUR")
        get_response = json.loads(get_response.text)
        assert get_response["status"] == "Completed"