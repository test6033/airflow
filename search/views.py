import requests
import threading
from forex_python.converter import CurrencyRates
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Search
from .serializers import SearchSerializer
from .currency import get_currency_list, get_currency_value


def request_task(url, data):
    requests.post(url, json=data)


def fire_and_forget(url, json):
    threading.Thread(target=request_task, args=(url, json)).start()


@api_view(["GET"])
def get(request, search_id=None, currency=None):
    if currency not in get_currency_list():
        return Response(status=404)

    try:
        search = Search.objects.get(search_id=search_id)
    except Exception as e:
        return Response({"message": str(e)}, status=404)

    serializer = SearchSerializer(search)

    currency_value = get_currency_value(currency)
    items = serializer.data["items"] if serializer.data["items"] else []
    results = []

    for item in items:
        if currency != item["pricing"]["currency"]:
            continue

        pricing = item["pricing"]
        item.update(
            {
                "price": {
                    "currency": "KZT",
                    "total": (
                        float(pricing["base"])
                        + float(pricing["taxes"])
                        + float(pricing["total"])
                    )
                    * currency_value,
                }
            }
        )
        results.append(item)

    return Response(
        {
            "id": serializer.data["id"],
            "search_id": serializer.data["search_id"],
            "status": serializer.data["status"],
            "items": sorted(
                results, key=lambda dictionary: dictionary["price"]["total"]
            ),
        }
    )


@api_view(["POST"])
def search(request):
    try:
        serializer = SearchSerializer(data=request.POST)
        serializer.is_valid(raise_exception=True)
        search = serializer.save()

        fire_and_forget(
            "http://185.146.3.44:8001/api/search", {"search_id": str(search.search_id)}
        )
        fire_and_forget(
            "http://185.146.3.44:8002/api/search", {"search_id": str(search.search_id)}
        )

        return Response(serializer.data, status=status.HTTP_201_CREATED)
    except Exception as e:
        return Response({"error_message": str(e)}, status=500)
