import pika, json

params = pika.URLParameters(
    "amqps://sdtevmrw:CER0nluE-IdH8vR_O0StXNjkOamBVONI@chimpanzee.rmq.cloudamqp.com/sdtevmrw"
)

connection = pika.BlockingConnection(params)

channel = connection.channel()


def publish(method, body):
    properties = pika.BasicProperties(method)
    channel.basic_publish(
        exchange="", routing_key="main", body=json.dumps(body), properties=properties
    )
