from django.urls import path

from .views import search, get

urlpatterns = [
    path("search", search),
    path("results/<str:search_id>/<str:currency>", get),
]
