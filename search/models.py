import uuid
from django.db import models


class Search(models.Model):
    class Meta:
        db_table = "searches"

    STATUSES = [
        ("Pending", "Pending"),
        ("Completed", "Completed"),
    ]

    search_id = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    status = models.CharField(
        max_length=15,
        choices=STATUSES,
        default="Pending",
    )

    items = models.JSONField(null=True)
