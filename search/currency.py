import requests
import xmltodict
import datetime
from django.core.cache import cache


TODAY = datetime.datetime.today()
TODAY_FORMAT = f"{TODAY.day}.{TODAY.month}.{TODAY.year}"
URL = f"https://www.nationalbank.kz/rss/get_rates.cfm?fdate={TODAY_FORMAT}"


def store_currency_values() -> list:
    response = requests.get(URL)
    data = xmltodict.parse(response.content)
    result = []
    for item in data["rates"]["item"]:
        result.append(
            {
                "currency": item["title"],
                "value": float(item["description"]),
            }
        )

    cache.set("currency_values", result, 86400)
    return result


def store_currency_list() -> list:
    response = requests.get(URL)
    data = xmltodict.parse(response.content)
    currencies = ["KZT"]
    for item in data["rates"]["item"]:
        currencies.append(item["title"])

    cache.set("currencies_list", currencies, 86400)
    return currencies


def get_currency_value(currency="RUB"):
    if currency == "KZT":
        return 1.0

    if cache.get("currency_values"):
        for item in cache.get("currency_values"):
            if currency == item["currency"]:
                return item["value"]
        else:
            return None


def get_currency_list():
    return cache.get("currencies_list")
