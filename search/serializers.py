from rest_framework import serializers
import asyncio
import aiohttp
from .models import Search


class SearchSerializer(serializers.ModelSerializer):
    class Meta:
        model = Search
        fields = "__all__"
